import { createGlobalStyle } from "styled-components";
import lvWebtoWoff from "./fonts/lvFont/louis_vuitton_font-webfont.woff";
import lvWebtoWoff2 from "./fonts/lvFont/louis_vuitton_font-webfont.woff2";



const FontStyles = createGlobalStyle`

@font-face {
  font-family: "futuramedium";
  src: url(${lvWebtoWoff2})
      format("woff2"),
    url(${lvWebtoWoff})
      format("woff");
  font-weight: normal;
  font-style: normal;
}

`;

export default FontStyles;

