import { GiHamburgerMenu } from "react-icons/gi/";
import { BsHandbag } from "react-icons/bs/";
import styled from "styled-components";
import Text from "../../../atoms/Text/Text";
import { createGlobalStyle } from "styled-components";

type Props = {};

const StyledFlex = styled.div`
  @media screen and (max-width: 390px) {
    display: flex;
    position: relative;

    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 50px;
    padding: 10px;
  }
`;

const TextFont = createGlobalStyle`
 @media screen and (max-width: 390px)
  {body{font-family: "futuramedium";
  font-style: bold;}}
`;

const Menu = createGlobalStyle`
 @media screen and (max-width: 390px)
  {body{font-family: "futuramedium";
  font-style: bold;}}
`;

const Test = (props: Props) => {
  return (
    <div>
      <StyledFlex>
        <GiHamburgerMenu />
        <TextFont />
        <Text value={"LOUIS VUITTON"} />
        <BsHandbag />
      </StyledFlex>
    </div>
  );
};

export default Test;
